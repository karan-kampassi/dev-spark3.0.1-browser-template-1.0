FROM python:3.7.3

WORKDIR /app

COPY . /app
                                                                         
ENTRYPOINT  ["python3"]

CMD ["main.py"]