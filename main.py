import sys
from operator import add

# Create SparkSession and sparkcontext
from pyspark.sql import SparkSession

spark = SparkSession.builder\
                    .appName('WordCountApplication')\
                    .getOrCreate()

sc=spark.sparkContext

# Read the input file and Calculating words count
text_file = sc.textFile("./data/sampleData.txt")
counts = text_file.flatMap(lambda line: line.split(" ")) \
             .map(lambda word: (word, 1)) \
             .reduceByKey(lambda a, b: a + b)

counts.saveAsTextFile("./data/sampleDataOutput.txt")

# Stopping Spark-Session and Spark context
sc.stop()
spark.stop()